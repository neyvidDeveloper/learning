<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 5/13/18
 * Time: 3:42 PM
 */

namespace App\Presenters;


use App\Models\Product\ProductStaus;
use App\Presenters\Contract\Presenter;

class ProductPresenter extends Presenter
{


    public function price_in_hezar_toman()
    {
        return number_format($this->entity->product_price ) . 'تومان';
    }

    public function show_status()
    {
        $status = $this->entity->product_status;
        $produtc_status_text=ProductStaus::getProductStatus($status);
        $product_status_css_class=ProductStaus::getProductStatusCssClass($status);
        echo "<span class='label label-".$product_status_css_class."'>".$produtc_status_text."</span>";
    }

    public function product_discount_whit_precent_character()
    {
        $product_discount=$this->entity->product_discount;
        return $product_discount.'%';
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/21/18
 * Time: 3:51 PM
 */

namespace App\Presenters\Order;


use App\Models\Order\Order;
use App\Models\Order\OrderStatus;
use App\Presenters\Contract\Presenter;
use App\Services\Payment\PaymentType;

class OrderPresenter extends Presenter
{
    public function orderStatus()
    {
        $orderStatus = $this->entity->order_status;
        return OrderStatus::getOrderStatus($orderStatus);

    }

    public function orderStatusHtml()
    {
        $orderStatus = $this->entity->order_status;
        $orderStatusText = OrderStatus::getOrderStatus($orderStatus);
        $orderStatusCssClass = OrderStatus::getOrderStatusCssClass($orderStatus);
        return "<span class='label label-" . $orderStatusCssClass . "'>" . $orderStatusText . "</span>";
    }

    public function opration()
    {
        $order = $this;
        return view('admin.order.opration', compact('order'));
    }

    public function isApproval()
    {
        return in_array($this->entity->order_status, [OrderStatus::UNPAID]);
    }

    public function payment_method()
    {
        return PaymentType::getPaymentType()[$this->entity->order_payment_method];
    }


}
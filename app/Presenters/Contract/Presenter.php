<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 5/13/18
 * Time: 3:32 PM
 */

namespace App\Presenters\Contract;


abstract class Presenter
{
    protected $entity;

    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    public function __get($property)
    {
        if (method_exists($this, $property)) {
            return $this->{$property}();
        }
        return $this->{$property};

    }

}
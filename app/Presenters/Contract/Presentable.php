<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 5/13/18
 * Time: 3:44 PM
 */

namespace App\Presenters\Contract;


use League\Flysystem\Exception;

trait Presentable
{
    protected $presenterInstance;

    public function present()

    {
        if (!$this->presenter || !class_exists($this->presenter)) {
            throw new \Exception('Presenter Not Found!');
        }
        $this->presenterInstance = new $this->presenter($this);
        return $this->presenterInstance;


    }

}
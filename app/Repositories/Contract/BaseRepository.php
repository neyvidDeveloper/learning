<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 5/13/18
 * Time: 11:16 AM
 */

namespace App\Repositories\Contract;


abstract class BaseRepository
{
    protected $model;

    public function __construct()
    {

    }

    public function find(int $id)
    {
        return $this->model::find($id);
    }

    public function all(array $eagerLoad = [])
    {
        if (!empty($eagerLoad)) {
            return $this->model::with($eagerLoad)->get();
        }
        return $this->model::all();
    }

    public function create(array $data)
    {
        return $this->model::create($data);
    }

    public function update(int $id, array $data)
    {
        $item = $this->find($id);
        return $item->update($data);
    }

    public function delete(int $id)
    {
        $item = $this->find($id);
        return $item->delete();
    }

    public function findBy(array $criteria, $relations = [], $single = true)
    {
        $query = $this->model::query();
        if (!is_null($relations)) {
            $query = $this->model::with($relations);
        }
        foreach ($criteria as $key => $value) {
            $query->where($key, $value);
        }
        if ($single) {
            return $query->first();
        }
        return $query->get();

    }
}
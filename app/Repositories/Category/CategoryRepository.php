<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 5/16/18
 * Time: 2:58 PM
 */

namespace App\Repositories\Category;


use App\Models\Category;
use App\Repositories\Contract\BaseRepository;

class CategoryRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Category::class;
    }

    public function getAttributeByCategoryId(int $categoryId)
    {
        return $this->model::find($categoryId)->attributes;
    }
}
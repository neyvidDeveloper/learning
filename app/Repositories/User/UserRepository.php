<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/25/18
 * Time: 10:04 AM
 */

namespace App\Repositories\User;


use App\Models\User;
use App\Repositories\Contract\BaseRepository;

class UserRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model=User::class;
    }
}
<?php


namespace App\Repositories\Attachment;


use App\Models\Attachment;
use App\Repositories\Contract\BaseRepository;

class AttachmentRepository extends BaseRepository
{
    function __construct()
    {
        parent::__construct();
        $this->model=Attachment::class;

    }
}
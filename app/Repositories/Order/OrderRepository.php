<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/11/18
 * Time: 2:26 PM
 */

namespace App\Repositories\Order;



use App\Models\Order\Order;
use App\Repositories\Contract\BaseRepository;

class OrderRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model=Order::class;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/11/18
 * Time: 3:16 PM
 */

namespace App\Repositories\Order;


use App\Models\Order\OrderItem;

use App\Repositories\Contract\BaseRepository;

class OrderItemRepository extends BaseRepository
{

    public function __construct()
    {
        parent::__construct();
        $this->model = OrderItem::Class;
    }
}
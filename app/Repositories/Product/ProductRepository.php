<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 5/13/18
 * Time: 11:16 AM
 */

namespace App\Repositories\Product;


use App\Models\Product;
use App\Repositories\Contract\BaseRepository;

class ProductRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Product::class;
    }



}
<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 5/24/18
 * Time: 4:49 PM
 */

namespace App\Repositories\Attribute;


use App\Models\Attribute;
use App\Models\AttributeCategory;
use App\Models\AttributeType;
use App\Repositories\Contract\BaseRepository;

class AttributeRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Attribute::class;
    }

    public function getAllTypes()
    {
        return AttributeType::get();
    }

    public function saveType(array $data)
    {
        return AttributeType::create($data);
    }
    public function getAllCategory()
    {
        return AttributeCategory::get();
    }

    public function saveCategory(array $data)
    {
        return AttributeCategory::create($data);
    }

}
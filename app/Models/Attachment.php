<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $primaryKey = 'attachment_id';
    protected $guarded = ['attachment_id'];

    public function products()
    {
        return $this->morphedByMany(Product::class,
            'attachable',
            'attachables',
            'attachable_id',
            'attachments_id');
    }
}

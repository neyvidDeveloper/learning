<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $primaryKey = 'discount_id';
    protected $dates = [
        'discount_start_date',
        'discount_end_date'
    ];
    public $guarded=['discount_id'];
}

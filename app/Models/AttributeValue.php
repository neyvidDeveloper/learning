<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    protected $primaryKey = 'attribute_values_id';
    protected $guarded = ['attribute_values_id'];

    public function attribute()
    {
        return $this->belongsTo(Attribute::class, 'attribute_values_id');
    }
}
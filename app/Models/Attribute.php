<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $primaryKey = 'attribute_id';
    protected $guarded = ['attribute_id'];
    public $timestamps = false;

    public function Category()
    {
        return $this->belongsTo(AttributeCategory::class, 'attribute_category_id');
    }

    public function Type()
    {
        return $this->belongsTo(AttributeType::class, 'attribute_type_id');
    }

    public function values()
    {
        return $this->hasMany(AttributeValue::class, 'attribute_values_id');
    }

    public function product_categories()
    {
        return $this->morphedByMany(Category::class, 'attributable', 'attributables', 'attributable_id', 'attribute_id');
    }

}

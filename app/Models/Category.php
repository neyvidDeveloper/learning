<?php

namespace App\Models;

use App\Traits\hasAttribute;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use hasAttribute;

    protected $primaryKey = 'category_id';
    protected $guarded = ['category_id'];
    public $timestamps = false;

    public function parent()
    {
        return $this->belongsTo(Category::class, 'category_parent_id', 'category_id');
    }

    public function childs()
    {
        return $this->hasMany(Category::class, 'category_parent_id', 'category_id');
    }

    public function products()
    {
//        return $this->belongsToMany(Product::class, 'product_category', 'category_id', 'product_id');
        return $this->hasMany(Product::class, 'product_category_id');
    }


}

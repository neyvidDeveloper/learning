<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/12/18
 * Time: 8:36 AM
 */

namespace App\Models\Order;


class OrderStatus
{
    const UNPAID = 0;
    const PAID = 1;
    const CANCELED = 2;
    const REFUNDED = 3;
    const DELIVERED = 4;
    const SENT = 5;
    const SEND_READY = 6;

    public static function getAllOrderStatus()
    {
        return [
            self::UNPAID => 'پرداخت نشده',
            self::PAID => 'پرداخت شده',
            self::CANCELED => 'کنسل شده',
            self::REFUNDED => 'برگشت داده شده',
            self::DELIVERED => 'تحویل داده شده',
            self::SENT => 'ارسال شده',
            self::SEND_READY => 'آماده ارسال',
        ];
    }

    public static function getOrderStatus(int $status)
    {
        return self::getAllOrderStatus()[$status];

    }

    public static function getOrderStatusCssClass(int $status)
    {
        return [
            self::UNPAID => 'primary',
            self::PAID => 'success',
            self::CANCELED => 'warning',
            self::REFUNDED => 'danger',
            self::DELIVERED => 'info',
            self::SENT => 'info',
            self::SEND_READY => 'info',
        ][$status];
    }
}
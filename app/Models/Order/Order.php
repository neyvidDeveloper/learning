<?php

namespace App\Models\Order;


use App\Models\User;
use App\Presenters\Contract\Presentable;
use App\Presenters\Order\OrderPresenter;
use App\Services\Payment\PaymentType;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use Presentable;
    protected $primaryKey = 'order_id';
    protected $guarded = ['order_id'];
    public $presenter = OrderPresenter::class;

    public function user()
    {
        return $this->belongsTo(User::class, 'order_user_id');
    }

    public function order_items()
    {
        return $this->hasMany(OrderItem::class, 'order_item_order_id');
    }

    public function order_logs()
    {
        return $this->hasMany(OrderLog::class, 'order_log_order_id');
    }

    public function getPaymentMethodAttribute()
    {
        $type = PaymentType::getPaymentType();
        return $type[$this->order_payment_method];
    }

    public function updateStatus(int $status)
    {
        $statuses=OrderStatus::getAllOrderStatus();
        if(!in_array($status,array_keys($statuses))){
            return false;
        }
        $this->order_status=$status;
        $this->save();
    }

}

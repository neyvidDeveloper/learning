<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $primaryKey = 'order_item_id';
    protected $guarded = ['order_item_id'];

    public function Order()
    {
        return $this->belongsTo(Order::class,'order_item_order_id');
    }
}

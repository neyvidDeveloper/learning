<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeTypeValue extends Model
{
    protected $guarded=['attribute_type_value_id'];
    protected $primaryKey='attribute_type_value_id';

    public function type()
    {
        return $this->belongsTo(AttributeType::class,'attribute_type_id');
    }
}

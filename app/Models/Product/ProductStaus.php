<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 5/15/18
 * Time: 5:58 PM
 */

namespace App\Models\Product;


class ProductStaus
{
    const SUCCESS = 1;
    const WARNING = 2;
    const PRIMARY = 3;

    public static function getProductStatuses()
    {
        return [
            self::SUCCESS => 'فعال',
            self::WARNING => 'غیر فعال',
            self::PRIMARY => 'درحال بررسی',
        ];
    }

    public static function getProductStatus(int $productStatus)
    {
        return self::getProductStatuses()[$productStatus];
    }

    public static function getProductStatusCssClass(int $productStatus)
    {
        return [
            self::SUCCESS => 'success',
            self::WARNING => 'warning',
            self::PRIMARY => 'primary'
        ][$productStatus];
    }


}
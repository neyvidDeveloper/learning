<?php

namespace App\Models;

use App\Models\Filters\QueryFilters;
use App\Presenters\Contract\Presentable;
use App\Presenters\ProductPresenter;
use App\Traits\hasAttachment;
use App\Traits\hasAttribute;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Presentable, hasAttribute, hasAttachment;
    protected $primaryKey = 'product_id';
    protected $guarded = ['product_id'];
    protected $presenter = ProductPresenter::class;

    public function setProductSlugAttribute()
    {
        $this->attributes['product_slug'] = preg_replace('/\s+/', '_', $this->attributes['product_title']);
    }

    public function category()
    {
//        return $this->belongsToMany(Category::class, 'product_category', 'product_id', 'category_id');
        return $this->belongsTo(Category::class, 'product_category_id');
    }

    public function getProductThumbnailAttribute()
    {
        $attachment = $this->attachments()->first();
        return config('upload.url') . optional($attachment)->attachment_name;
    }

    public function scopeFilter($query,QueryFilters $filters)
    {
        return $filters->apply($query);
    }
}

<?php


namespace App\Models\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class QueryFilters
{
    protected $request;
    protected $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        foreach ($this->filter() as $key => $value) {
            if (!method_exists($this, $key)) {
                continue;
            }
            !empty($value) && strlen($value) > 0 ? $this->{$key}($value) : $this->{$key}();
        } return $this->builder;


    }

    public function filter()
    {
        return $this->request->all();
    }

}
<?php


namespace App\Models\Filters;


class ProductFilter extends QueryFilters
{

    public function minPrice($value='')
    {

        $this->builder->where('product_price','<=',$value);
    }

}
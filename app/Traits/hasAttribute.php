<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/9/18
 * Time: 9:09 AM
 */

namespace App\Traits;


use App\Models\Attribute;

trait hasAttribute
{
    public function attributes()
    {
        return $this->morphToMany(Attribute::class,
            'attributable',
            'attributables',
            'attributable_id',
            'attribute_id')->withPivot('attribute_value');
    }

}
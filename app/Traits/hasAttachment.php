<?php


namespace App\Traits;


use App\Models\Attachment;

trait hasAttachment
{

    public function attachments()
    {
        return $this->morphToMany(Attachment::class,
            'attachable',
            'attachables',
            'attachable_id',
            'attachment_id')->withPivot('type_of_attachment');
    }
}
<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\UserCreate;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function showRegister()
    {
        $title = 'فرم ثبت نام';
        return view('frontend.user.registerForm', compact('title'));
    }

    public function doRegister(UserCreate $request)
    {
//        $request->validate([
//            'user_name'=>'required',
//            'user_email'=>'required|E-mail',
//            'user_password'=>'required',
//        ],[
//            'user_name.required'=>'نام و نام خانوادگی خود را وارد نمایید',
//            'user_email.required'=>'لطفا ایمیل خود را وارد نمایید',
//            'user_password.required'=>'لطفا رمز عبور خود را وارد نمایید',
//        ]);

        UserService::create($request->all());
    }

    public function showLogin()
    {
        $title = 'فرم ورود به سایت';
        return view('frontend.user.loginForm', compact('title'));
    }
    public function doLogin(Request $request)
    {
       if(Auth::attempt(['email'=>$request->input('user_email'),'password'=>$request->input('user_password')])){
          return redirect('/');
       }

    }
    public function doLogOut()
    {
            Auth::logout();
            return redirect()->route('home');
    }
}

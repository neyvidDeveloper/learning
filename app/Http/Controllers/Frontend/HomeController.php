<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Filters\ProductFilter;
use App\Models\Product;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    protected $productRepository;

    public function __construct()
    {
        $this->productRepository = new ProductRepository();
    }

    public function index(ProductFilter $filter)
    {
        $title = 'فروشگاه نوید';
//        dd(Product::filter($filter));
        $products = Product::filter($filter)->get();
        $products = Cache::remember('products', 60,function () use ($filter) {
            return $products = Product::filter($filter)->get();
        });


        return view('frontend.home.index', compact('title', 'products'));
    }
//    public function index(Request $request)
//    {
//        $title = 'فروشگاه نوید';
//        $query=Product::query();
//
//        if ($request->has('minPrice')) {
//            $query->where('product_price','>=',$request->input('minPrice'));
//        }
//        if ($request->has('maxPrice')) {
//            $query->where('product_price','<=',$request->input('maxPrice'));
//        }
//        $products = $query->get();
//        return view('frontend.home.index', compact('title', 'products'));
//    }
}

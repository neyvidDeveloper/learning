<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    protected $productRepository;
    public function __construct()
    {
        $this->productRepository=new ProductRepository();
    }
    public function singlePage($product_id)
    {
        $title='محصول';
        $productInfo=$this->productRepository->find($product_id);
        return view('frontend.product.single',compact('title','productInfo'));
    }
}

<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\Product\ProductRepository;
use App\Services\Basket\BasketService;
use App\Services\Order\OrderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BasketController extends Controller
{

    public function __construct()
    {
    }

    public function add(int $product_id)
    {
        BasketService::add($product_id);

    }

    public function show()
    {
        $title='سبد خرید شما';
        $basket=session()->get('basket.items');
        return view('frontend.basket.show',compact('basket','title'));
    }

    public function update(Request $request)
    {

        $product_id=$request->input('product_id');
        $action=$request->input('action');
        $result=BasketService::Update($product_id,$action);
        if($result['success']){
            $result['subTotal']=BasketService::subTotal($product_id);
            $result['total']=BasketService::total();

        }
        return $result;
    }

    public function checkOut()
    {
        $basket=session()->get('basket.items');
        $orderData=[
            'user_id' => 1,
            'total_amount' => BasketService::total(),
            'order_discount' => 0,
            'order_items'=>[]
        ];
        foreach ($basket as $product_id=>$product){
            $orderData['order_items'][]=[
                'product_id'=>$product_id,
                'amount'=>$product['product_item']->product_price,
                'disount'=>$product['product_item']->product_discount,
            ];
        }
        $orderService=new OrderService();
        $orderService->create($orderData);

    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Repositories\Attribute\AttributeRepository;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public $repository;
    protected $attributeRepository;

    public function __construct()
    {
        $this->repository = new CategoryRepository();
        $this->attributeRepository=new AttributeRepository();

    }

    public function index()
    {
        $title = 'لیست دسته بندی ها';
        $category_item = $this->repository->all(['parent']);
        return view('admin.category.index', compact('title', 'category_item'));
    }

    public function create()
    {
        $title = 'ایجاد دسته بندی جدید';
        $category_childs = $this->repository->all();
        $attributes=$this->attributeRepository->all();
        return view('admin.category.create', compact('title', 'category_childs','attributes'));


    }

    public function store(Request $request)
    {
        dd($request->input('attribute_item'));
        $categoryInfo = [
            'category_title' => $request->input('category_title'),
            'category_parent_id' => $request->input('category_sub')
        ];
        $result = $this->repository->create($categoryInfo);
        if ($result && $result instanceof Category) {
            $result->attributes()->sync($request->input('attribute_item'));
            return redirect()->back();
        }
    }

    public function edit(Request $request)
    {
        $title = 'ویرایش دسته بندی ها';
        $category_info = $this->repository->find($request->input('catid'));
        $all_category = $this->repository->all();
        return view('admin.category.edit', compact('category_info', 'all_category', 'title'));
    }

    public function update(Request $request)
    {
        $category_update_data=[
            'category_title' => $request->input('category_title'),
            'category_parent_id' => $request->input('category_parent_id')
        ];
        $category_update_id=$request->input('catid');
        $result=$this->repository->update($category_update_id,$category_update_data);
        if($result){
            return redirect()->back()->with('success','دسته بندی با موقیت بروزرسانی گردید');
        }


    }
}

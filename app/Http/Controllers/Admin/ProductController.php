<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductCreate;
use App\Models\Product;
use App\Repositories\Attachment\AttachmentRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use App\Services\Product\ProductService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new ProductRepository();
    }

    public function index()
    {
        $title = 'نمایش محصولات';
        $product_items = $this->repository->all();
        return view('admin.product.index', compact('title', 'product_items'));
    }

    public function create()
    {
        $title = 'ایجاد محصول جدید';
        $categories = (new CategoryRepository())->all()->groupBy('category_parent_id');
        return view('admin.product.create', compact('title', 'categories'));
    }

    public function store(Request $request)
    {

        $product_data = [
            'product_code' => 0,
            'product_category_id' => $request->input('product_category'),
            'product_title' => $request->input('product_title'),
            'product_slug' => '',
            'product_price' => $request->input('product_price'),
            'product_stock' => $request->input('product_stock'),
            'product_discount' => $request->input('product_discount'),
            'product_type' => $request->input('product_type'),
            'product_coupon_count' => $request->input('product_coupon_count'),
            'product_description' => $request->input('product_description'),
            'product_visible' => $request->exists('product_visible'),
            'product_file' => $request->file('product_file')
        ];
        $result = ProductService::create($product_data);
        if ($result['productCreate'] && $result['productCreate'] instanceof Product) {
            $attributes = $request->input('product_attributes');
            $attributeValues = [];
            foreach ($attributes as $attribute_id => $attribute_value) {
                $attributeValues[$attribute_id] = ['attribute_value' => $attribute_value];
            }
            $result['productCreate']->attributes()->sync($attributeValues);
            $attachmentRepository = new AttachmentRepository();
            $attachmentCreate=$attachmentRepository->create([
                'attachment_type' => $result['file']['fileType'],
                'attachment_name' => $result['file']['fileName'],
                'attachment_size' => $result['file']['fileSize'],
            ]);
            $attachment[$attachmentCreate->attachment_id]=['type_of_attachment'=>'tumbnail'];
            $result['productCreate']->attachments()->sync($attachment);
            return redirect('admin/product/create')->with('success', true);
        }
    }

    public function edit(Request $request)
    {
        $title = 'ویرایش محصول';
        $product_Item = $request->input('id');
        $categories = (new CategoryRepository())->all()->groupBy('category_parent_id');
        $productInfo = $this->repository->find($product_Item);
        $product_category_relation = $productInfo->categories()->get()->groupBy('category_id');
        return view('admin.product.edit', compact('title', 'productInfo', 'categories', 'product_category_relation'));
    }

    public function update(ProductCreate $request)
    {
//       $request->validate([
//           'product_title'=>'required',
//           'product_price'=>'required|regex:/^\d+$/'
//       ],[
//           'product_title.required'=>'نام محصول را وارد نمایید',
//           'product_price.required'=>'قیمت را وارد نمایید',
//           'product_price.regex'=>'قیمت باید درست وارد شود'
//       ]);
        $productUpdateData = [
            'product_code' => 0,
            'product_title' => $request->input('product_title'),
            'product_slug' => '',
            'product_price' => $request->input('product_price'),
            'product_stock' => $request->input('product_stock'),
            'product_discount' => $request->input('product_discount'),
            'product_coupon_count' => $request->input('product_coupon_count'),
            'product_description' => $request->input('product_description'),
            'product_visible' => $request->exists('product_visible'),
        ];
        $this->repository->find($request->input('id'))->categories()->sync($request->input('product_category'));
        $result = $this->repository->update($request->input('id'), $productUpdateData);

        if ($result) {

            return redirect('admin/product');
        }
    }

    public function delete(Request $request)
    {
        $delet_relation_result = $this->repository->find($request->input('pid'))->categories()->detach();
        $result = $this->repository->delete($request->input('pid'));
        if ($result && $delet_relation_result) {
            return redirect('admin/product');
        }
    }





}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Attribute;
use App\Models\AttributeCategory;
use App\Models\AttributeType;
use App\Repositories\Attribute\AttributeRepository;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttributeController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new AttributeRepository();
    }

    public function index()
    {
        $title = 'لیست ویژگی ها';
        $attributes = $this->repository->all();
        return view('admin.attribute.index', compact('title', 'attributes'));
    }

    public function create()
    {
        $title = 'ایجاد ویژگی جدید';
        $allTypes = $this->repository->getAllTypes();
        $allAttrCategory = $this->repository->getAllCategory();
        return view('admin.attribute.create', compact('title', 'allTypes', 'allAttrCategory'));
    }

    public function store(Request $request)
    {

        $attribute_date = [
            'attribute_name' => $request->input('attribute_name'),
            'attribute_type_id' => $request->input('attribute_type_id'),
            'attribute_category_id' => $request->input('attribute_category_id')
        ];
        $result = $this->repository->create($attribute_date);
        if ($result && $result instanceof Attribute) {
            return redirect()->back()->with('success', 'ویژگی با موفقیت ثبت گردید');
        }
    }

    public function index_type()
    {
        $allTypes = $this->repository->getAllTypes();
        $title = 'لیست نوع ویژگی';
        return view('admin.attribute_type.index', compact('title', 'allTypes'));
    }

    public function create_type()
    {
        $title = 'ایجاد نوع ویژگی جدید';
        return view('admin.attribute_type.create', compact('title'));
    }

    public function store_type(Request $request)
    {
        $type_data = [
            'attribute_type_title' => $request->input('attribute_type_title')
        ];
        $result = $this->repository->saveType($type_data);
        if ($result && $result instanceof AttributeType) {
            return redirect()->back()->with('success', 'نوع ویژگی با موفقیت ثبت گردید');
        }
    }

    public function index_category()
    {
        $title = 'لیست دسته بندی ویژگی ها';
        $allCategory = $this->repository->getAllCategory();
        return view('admin.attribute_category.index', compact('title', 'allCategory'));

    }

    public function create_category()
    {
        $title = 'ایجاد دسته بندی جدید برای ویژگی';
        return view('admin.attribute_category.create', compact('title'));
    }

    public function store_category(Request $request)
    {
        $category_data = [
            'attribute_category_title' => $request->input('attribute_category_title')
        ];
        $result = $this->repository->saveCategory($category_data);
        if ($result && $result instanceof AttributeCategory) {
            return redirect()->back()->with('success', ' دسته بندی ویژگی با موفقیت ثبت گردید');
        }
    }

    public function getCategoryAttribute(Request $request,$pid)
    {
       $categoryRepository=new CategoryRepository();
       $attributes=$categoryRepository->getAttributeByCategoryId($pid);
       $attrHtml=view('admin.attribute.render.item',compact('attributes'))->render();
       return $attrHtml;
    }
}

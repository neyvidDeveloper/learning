<?php

namespace App\Http\Controllers\Admin;

use App\Events\Event;
use App\Events\OrderCreated;
use App\Jobs\SendInvoice;
use App\Models\Order\OrderStatus;
use App\Repositories\Order\OrderRepository;
use App\Services\Order\OrderService;
use App\Services\Payment\PaymentService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    protected $orderRepository;
    protected $productRepository;
    protected $orderService;


    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
        $this->orderService=new OrderService();

    }

    public function index()
    {
        $orders = $this->orderRepository->all(['user','order_items']);
        $title = 'لیست سفارش ها';
        return view('admin.order.index', compact('orders', 'title'));
    }

    public function create()
    {
//        $title = 'ایجاد سفارش جدید';
//        $allProduct=$this->productRepository->all();
//        return view('admin.order.create', compact('title','allProduct'));
        $orderService = new OrderService();
        $order = $orderService->create([
            'user_id' => 1,
            'total_amount' => 1500000,
            'order_discount' => 0,
            'order_items' => [
                [
                    'product_id' => 7,
                    'amount' => 500000,
                    'disount' => 0
                ], [
                    'product_id' => 8,
                    'amount' => 600000,
                    'disount' => 0
                ], [
                    'product_id' => 9,
                    'amount' => 700000,
                    'disount' => 0
                ],
            ]
        ]);

//        SendInvoice::dispatch($order)->delay(now()->addMinutes(10))->onQueue('email')->onConnection('sqs');
        event(new OrderCreated($order['order']));

    }

    public function payment(Request $request, $orderCode)
    {
        $orderInfo = $this->orderRepository->findBy(['order_code' => $orderCode], ['user']);
        $paymentService = new PaymentService();
        $paymentResult = $paymentService->pay($orderInfo);
    }

    public function approve(Request $request,$order_id)
    {
      $order_item=$this->orderRepository->find($order_id);
      $order_item->updateStatus(OrderStatus::PAID);
      $this->orderService->updateStatus($order_id,$order_item->order_status,OrderStatus::PAID);
    }
}

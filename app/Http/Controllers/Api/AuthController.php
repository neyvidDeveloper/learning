<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MiladRahimi\LaraJwt\Facades\JwtAuth;


class AuthController extends Controller
{
    public function Login(Request $request)
    {
//

        if (Auth::guard('api')->attempt([
            'email' => $request->input('user_email'),
            'password' => $request->input('user_password')
        ])) {
            $user = Auth::guard('api')->user();
            $token=JwtAuth::generateToken($user);
            return [
                'token' => $token
            ];
        }
        return [
            'message' => false
        ];
    }

}

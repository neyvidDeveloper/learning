<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_title' => 'required',
            'product_price' => 'required|regex:/^\d+$/'
        ];
    }

    public function messages()
    {
        return [
            'product_title.required' => 'نام محصول را وارد نمایید',
            'product_price.required' => 'قیمت را وارد نمایید',
            'product_price.regex' => 'قیمت باید درست وارد شود'
        ];
    }
}

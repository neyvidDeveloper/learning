<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'user_name' => 'required',
            'user_email' => 'required|E-mail',
            'user_password' => 'required',


        ];
    }

    public function messages()
    {
        return [
            'user_name.required' => 'نام و نام خانوادگی خود را وارد نمایید',
            'user_email.required' => 'لطفا ایمیل خود را وارد نمایید',
            'user_password.required' => 'لطفا رمز عبور خود را وارد نمایید',
        ];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/12/18
 * Time: 7:23 PM
 */

namespace App\Services\Payment;


use App\Models\Order\Order;

class PaymentService
{
    public function __construct()
    {

    }

    public function pay(Order $order)
    {
        $paymentClass = PaymentType::getPaymentClass($order->order_payment_method);
        $paymentClassInstance = new $paymentClass;
        $paymentClassInstance->pay($order->order_id);

    }

}
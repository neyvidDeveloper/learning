<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/17/18
 * Time: 10:06 AM
 */

namespace App\Services\Payment;


use App\Services\Payment\PaymentMethods\Account;
use App\Services\Payment\PaymentMethods\Cod;
use App\Services\Payment\PaymentMethods\Online;

class PaymentType
{
    const ONLINE = 1;
    const ACCOUNT = 2;
    const COD = 3;


    public static function getPaymentType()
    {
        return [
            self::ONLINE => 'درگاه های آنلاین',
            self::ACCOUNT => 'حساب کاربری',
            self::COD => 'پرداخت در محل'
        ];

    }

    public static function getPaymentClass(int $paymentType)
    {
        return [
            self::ONLINE => Online::class,
            self::ACCOUNT => Account::class,
            self::COD => Cod::class
        ][$paymentType];

    }

}

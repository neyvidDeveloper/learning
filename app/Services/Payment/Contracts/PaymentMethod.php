<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/12/18
 * Time: 7:25 PM
 */

namespace App\Services\Payment\Contracts;


use App\Repositories\Order\OrderRepository;

abstract class PaymentMethod
{
    protected $orderRepository;

    public function __construct()
    {
        $this->orderRepository=new OrderRepository();
    }
    abstract public function pay(int $orderId);
}
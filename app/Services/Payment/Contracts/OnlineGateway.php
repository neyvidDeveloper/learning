<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/13/18
 * Time: 8:28 AM
 */

namespace App\Services\Payment\Contracts;


abstract class OnlineGateway
{
    abstract public function PaymentRequest(array $params);
    abstract public function VerifyPayment();


}
<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/12/18
 * Time: 7:33 PM
 */

namespace App\Services\Payment\PaymentMethods;


use App\Models\Order\OrderStatus;
use App\Services\Order\OrderService;
use App\Services\Payment\Contracts\PaymentMethod;

class Account extends PaymentMethod
{

    public function pay(int $orderId)
    {
        $orderItems = $this->orderRepository->find($orderId);
        $orderOwner = $orderItems->user;
        if ($orderOwner->balance >= $orderItems->order_total_amount) {
            $orderItems->order_status = OrderStatus::PAID;
            $orderItems->save();
            $orderOwner->decrement('balance', $orderItems->order_total_amount);
            $orderService = new OrderService();
            $updateResult = $orderService->updateStatus($orderId, OrderStatus::UNPAID, OrderStatus::PAID);
            if ($updateResult) {
                return [
                    'status' => true,
                    'order_id' => $orderId
                ];
            }
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/23/18
 * Time: 7:10 PM
 */

namespace App\Services\Basket;


use App\Repositories\Product\ProductRepository;

class BasketService
{
    public static function add(int $product_id)
    {
        $productRepository = new ProductRepository();
        $product_item = $productRepository->find($product_id);
        if ($product_item) {
            $items = self::Items();

            if (is_null($items)) {
                $items = [];
            }
            if (isset($items[$product_id])) {
                $items[$product_id]['count'] += 1;
            } else {
                $items[$product_id] = [
                    'count' => 1,
                    'product_item' => $product_item
                ];
            }
            session(['basket.items' => $items]);
        }
//        session()->push('basket.items', [$product_id =>
//            [
//                'count' => 1,
//                'product_item' => $product_item
//            ]
//        ]);


    }

    public static function Items()
    {
        return session('basket.items');
    }

    public static function Update($product_id, $action)
    {

        $items = self::Items();
        if (isset($items[$product_id])) {
            $product = $items[$product_id]['product_item'];
            $product = $product->fresh();
            $current_count = $items[$product_id]['count'];
            if ($action == 'plus') {
                if ($current_count < $product->product_stock) {
                    $items[$product_id]['count'] += 1;
                    $items[$product_id]['product_item'] = $product;
                    session(['basket.items' => $items]);
                } else {
                    return [
                        'success' => false,
                        'message' => 'تعداد بیشتر فایل انتخاب نمی باشد',
                    ];
                }
            }
            if ($action == 'minus') {
                if ($current_count > 1) {
                    $items[$product_id]['count'] -= 1;
                    $items[$product_id]['product_item'] = $product;
                    session(['basket.items' => $items]);

                } else {
                    return [
                        'success' => false,
                        'message' => 'کمتر از یک عدد قابل سفارش نیست',
                    ];
                }
            }
            return ['success' => true];
        }
        return [
            'success' => false,
            'message' => 'محصل مودر نظر موجود نمی باشد'

        ];
    }

    public static function total()
    {
        return collect(self::items())->reduce(function ($total, $item) {
            return $total + ($item['product_item']->product_price * $item['count']);
        }, 0);

    }

    public static function subTotal(int $product_id)
    {
        $items = self::Items();
        if (isset($items[$product_id])) {
            return $items[$product_id]['product_item']->product_price * $items[$product_id]['count'];
        }
        return 0;
    }
}
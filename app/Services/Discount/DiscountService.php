<?php


namespace App\Services\Discount;


use App\Models\Discount;
use App\Services\Exception\InvalidDateRangeException;

class DiscountService
{
    public function create(array $data)
    {
        $startdate=$data['discount_start_date'];
        $endDate=$data['discount_end_date'];
        if($startdate->gt($endDate)){

           throw new InvalidDateRangeException('تاریخ های تخفیف نا معتبر می باشد ');
        }

        $newDiscount=Discount::create($data);
        return $newDiscount;

    }
    public function isValid()
    {

    }

}
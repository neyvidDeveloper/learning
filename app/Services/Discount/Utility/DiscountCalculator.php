<?php


namespace App\Services\Discount\Utility;


class DiscountCalculator
{
    public static function calculateDiscountByPercent(int $percent, int $price)
    {
        if($percent==0){
            return $price;
        }
        return (1 - ($percent / 100)) * $price;
    }

    public static function calculateDiscountByValue(int $value, int $price)
    {
        return $price - $value;
    }

}
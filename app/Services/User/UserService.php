<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/25/18
 * Time: 9:55 AM
 */

namespace App\Services\User;


use App\Events\UserRegistered;
use App\Models\User;
use App\Repositories\User\UserRepository;

class UserService
{
    public static function create(array $data)
    {
        $userRepository = new UserRepository();
        $userCreated = $userRepository->create([
            'name' => $data['user_name'],
            'email' => $data['user_email'],
            'password' => $data['user_password'],
        ]);
        if ($userCreated && $userCreated instanceof User){
            event(new UserRegistered($userCreated));
        }
        return $userCreated;
    }
}
<?php


namespace App\Services\Upload;


use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;


class UploadService
{
    public static function upload(UploadedFile $uploadedFile)
    {
        $fileExtension = $uploadedFile->getClientOriginalExtension();
        $newName = str_random(10);
        $fileName = $newName . '.' . $fileExtension;
        $newFile = $uploadedFile->move(config('upload.path'), $fileName);
        $optimizedFile=Image::make($newFile->getRealPath())->resize(150, 150)->save();
        return array(
            'fileType'=>$optimizedFile->mime(),
            'fileName' => $newFile->getFilename(),
            'fileSize'=>$optimizedFile->filesize()
        );
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 6/11/18
 * Time: 9:52 AM
 */

namespace App\Services\Order;


use App\Events\OrderCreated;
use App\Models\Order\OrderStatus;
use App\Models\Order\OrderLog;
use App\Repositories\Order\OrderItemRepository;
use App\Repositories\Order\OrderLogRepository;
use App\Repositories\Order\OrderRepository;
use App\Services\Discount\DiscountService;
use App\Services\Order\UpdateStatus\CheckAgent;
use App\Services\Order\UpdateStatus\CheckOrder;
use App\Services\Payment\PaymentType;

class OrderService
{
    private $orderRepository;
    private $orderItemRepository;
    private $orderLogRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
        $this->orderItemRepository = new OrderItemRepository();
        $this->orderLogRepository = new OrderLogRepository();

    }

    public function create(array $data)
    {
        $newOrder = $this->orderRepository->create([
            'order_code' => str_random(10),
            'order_user_id' => $data['user_id'],
            'order_total_amount' => $data['total_amount'],
            'order_discount' => $data['order_discount'],
            'order_payable_amount' => DiscountService::calculateDiscountByPercent($data['order_discount'], $data['total_amount']),
            'order_payment_method' => PaymentType::ACCOUNT,
            'order_status' => OrderStatus::UNPAID,
        ]);
        $orderItems = [];
        foreach ($data['order_items'] as $item) {
            $orderItems[] = [
                'order_item_order_id' => $newOrder->id,
                'order_item_product_id' => $item['product_id'],
                'order_item_amount' => $item['amount'],
                'order_item_product_discount' => $item['disount'],
                'order_item_payable_amount' => DiscountService::calculateDiscountByPercent($item['disount'], $item['amount'])
            ];
        }
        $createOrderItem = $newOrder->order_items()->createMany($orderItems);
        if ($createOrderItem) {
            event(new OrderCreated($newOrder));
            return [
                'success' => true,
                'order' => $newOrder,
                'order_items' => $createOrderItem
            ];
        }

    }

    public function updateStatus(int $orderId, int $current_status, int $next_status)
    {
        $data = [
            'order_id' => $orderId,
            'current_status' => $current_status,
            'next_status' => $next_status,
        ];
        $checkAgent = new CheckAgent();
        $checkOrder = new CheckOrder($checkAgent);
        $checkOrder->handle($data);

        $currentUser = 1;
        $result = $this->orderLogRepository->create([
            'order_log_order_id' => $orderId,
            'order_log_current_status' => $current_status,
            'order_log_next_status' => $next_status,
            'order_log_agent' => $currentUser,
        ]);
        return $result instanceof OrderLog;
    }


}
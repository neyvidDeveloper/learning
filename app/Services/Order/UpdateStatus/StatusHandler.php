<?php


namespace App\Services\Order\UpdateStatus;


abstract class StatusHandler
{
    protected $next;

    public function __construct(StatusHandler $handler = null)
    {
        $this->next = $handler;
    }

    public function handle(array $data)
    {
        $processed = $this->process($data);
        if ($processed) {
            if ($this->next !== null) {
                $processed = $this->next->handle($data);
            }
        }
        return $processed;

    }

    abstract public function process(array $data);
}
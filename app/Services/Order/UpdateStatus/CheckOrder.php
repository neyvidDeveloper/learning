<?php


namespace App\Services\Order\UpdateStatus;


use App\Models\Order\Order;
use App\Repositories\Order\OrderRepository;

class CheckOrder extends StatusHandler
{

    public function process(array $data)
    {
        $orderRepository=new OrderRepository();
        $orderItem=$orderRepository->find($data['order_id']);
        if($orderItem && $orderItem instanceof Order){
            return true;
        }
        throw new \Exception('سفارش مورد نظر مورد تایید نمی باشد');
    }
}
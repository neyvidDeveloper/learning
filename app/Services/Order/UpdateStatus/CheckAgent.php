<?php


namespace App\Services\Order\UpdateStatus;


use App\Models\Order\OrderStatus;
use Illuminate\Support\Facades\Auth;

class CheckAgent extends StatusHandler
{

    public function process(array $data)
    {
        $currentUser = Auth::user();
        if ($data['next_status'] == OrderStatus::REFUNDED && $currentUser->role != 'admin') {
            throw new \Exception('شما مجاز به انجام این عملیات نیستید');
        }
        return true;
    }
}
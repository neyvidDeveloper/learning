<?php


namespace App\Services\Product;


use App\Repositories\Product\ProductRepository;
use App\Services\Upload\UploadService;

class ProductService
{
    public static function create(array $data)
    {

        $productReository = new ProductRepository();
        $productCreate = $productReository->create([
            'product_code' => 0,
            'product_category_id' => $data['product_category_id'],
            'product_title' => $data['product_title'],
            'product_slug' => '',
            'product_price' => $data['product_price'],
            'product_stock' => $data['product_stock'],
            'product_discount' => $data['product_discount'],
            'product_type' => $data['product_type'],
            'product_coupon_count' => $data['product_coupon_count'],
            'product_description' => $data['product_description'],
            'product_visible' => $data['product_visible'],
        ]);
        if ($data['product_file']) {
            $file = UploadService::upload($data['product_file']);
        }
        return
            [
                'file' => $file,
                'productCreate' => $productCreate
            ];

    }

}
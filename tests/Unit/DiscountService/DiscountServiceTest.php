<?php


namespace Tests\Unit\DiscountService;


use App\Services\Discount\DiscountService;
use Tests\TestCase;

class DiscountServiceTest extends TestCase
{

    
    
    /** @test */
    public function it_can_init_discount_service()
    {

        $discounService = new DiscountService();
        $this->assertInstanceOf(DiscountService::class, $discounService);
    }

    /** @test */
    public function it_has_create_method()
    {
        $discounService = new DiscountService();
        $this->assertTrue(method_exists($discounService, 'create'));
    }

    /** @test */
    public function it_has_is_valid_method()
    {
        $discounService = new DiscountService();
        $this->assertTrue(method_exists($discounService, 'isValid'));
    }
}
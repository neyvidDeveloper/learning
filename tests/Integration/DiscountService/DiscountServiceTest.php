<?php


namespace Tests\Integration\DiscountServiceTest;


use App\Models\Discount;
use App\Services\Discount\DiscountService;
use App\Services\Exception\InvalidDateRangeException;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;


class DiscountServiceTest extends TestCase
{
    use RefreshDatabase;
    protected $discountService;

    public function __construct()
    {
        parent::__construct();
        $this->discountService = new DiscountService();
    }

    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /** @test */
    public function it_can_create_new_discount()
    {

        $params = [
            'discount_code' => str_random(5),
            'discount_percent' => 10,
            'discount_target' => 0,
            'discount_start_date' => Carbon::now()->addDay(5),
            'discount_end_date' => Carbon::now()->addDay(10),
            'discount_limit' => 10
        ];
        $newDiscount = $this->discountService->create($params);
        $this->assertInstanceOf(Discount::class, $newDiscount);
    }

    /** @test */
    public function it_can_validate_discount_date()
    {
        $this->expectException(InvalidDateRangeException::class);
        $params = [
            'discount_code' => str_random(5),
            'discount_percent' => 10,
            'discount_target' => 0,
            'discount_start_date' => Carbon::now()->addDay(15),
            'discount_end_date' => Carbon::now()->addDay(10),
            'discount_limit' => 10
        ];
        $newDiscount = $this->discountService->create($params);
    }
}
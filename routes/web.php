<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('/admin','Admin\\DashboardController@index')->name('admin.dashboard');
//Route::get('/admin/product','Admin\\ProductController@index')->name('admin.product');
//Route::get('/admin/product/create','Admin\\ProductController@create')->name('admin.product.create');
//Route::post('/admin/product/create','Admin\\ProductController@store')->name('admin.product.store');
//Route::get('/admin/product/edit/{id?}','Admin\\ProductController@edit')->name('admin.product.edit');
//Route::post('/admin/product/edit','Admin\\ProductController@update')->name('admin.product.update');
//Route::get('/admin/product/delete/{pid?}','Admin\\ProductController@delete')->name('admin.product.delete');
//Route::get('/admin/category','Admin\\CategoryController@index')->name('admin.category');
//Route::get('/admin/category/create','Admin\\CategoryController@create')->name('admin.category.create');
//Route::post('/admin/category/create','Admin\\CategoryController@store')->name('admin.category.store');
//Route::get('/admin/category/edit/{catid?}','Admin\\CategoryController@edit')->name('admin.category.edit');
//Route::post('/admin/category/edit','Admin\\CategoryController@update')->name('admin.category.updaye');
Route::group(['namespace' => 'Frontend\\'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/basket/add/{pid}', 'BasketController@add')->name('basket.add');
    Route::get('/basket', 'BasketController@show')->name('basket.show');
    Route::post('/basket/update', 'BasketController@update')->name('basket.update');
    Route::get('/basket/checkout', 'BasketController@checkout')->name('basket.checkout');
    Route::get('/register', 'AuthController@showRegister')->name('registerForm');
    Route::post('/register', 'AuthController@doRegister')->name('doRegister');
    Route::get('/login', 'AuthController@showLogin')->name('loginForm');
    Route::post('/login', 'AuthController@doLogin')->name('doLogin');
    Route::get('logout', 'AuthController@doLogOut')->name('LogOut');
    Route::get('product/{pid}', 'ProductController@singlePage')->name('product.single');
});

Route::group(['prefix' => 'admin/', 'namespace' => 'Admin\\'], function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::get('product', 'ProductController@index')->name('admin.product');
    Route::get('product/create', 'ProductController@create')->name('admin.product.create');
    Route::post('product/create', 'ProductController@store')->name('admin.product.store');
    Route::get('product/edit/{id?}', 'ProductController@edit')->name('admin.product.edit');
    Route::post('product/edit', 'ProductController@update')->name('admin.product.update');
    Route::get('product/delete/{pid?}', 'ProductController@delete')->name('admin.product.delete');
    Route::get('category', 'CategoryController@index')->name('admin.category');
    Route::get('category/create', 'CategoryController@create')->name('admin.category.create');
    Route::post('category/create', 'CategoryController@store')->name('admin.category.store');
    Route::get('category/edit/{catid?}', 'CategoryController@edit')->name('admin.category.edit');
    Route::post('category/edit', 'CategoryController@update')->name('admin.category.updaye');
    Route::get('attribute', 'AttributeController@index')->name('admin.attribute');
    Route::get('attribute/create', 'AttributeController@create')->name('admin.attribute.create');
    Route::post('attribute/create', 'AttributeController@store')->name('admin.attribute.store');
    Route::get('attributecategory', 'AttributeController@index_category')->name('admin.attributecategory');
    Route::get('attributecategory/create', 'AttributeController@create_category')->name('admin.attributecategory.create');
    Route::post('attributecategory/create', 'AttributeController@store_category')->name('admin.attributecategory.store');
    Route::get('attributetype', 'AttributeController@index_type')->name('admin.attributetype');
    Route::get('attributetype/create', 'AttributeController@create_type')->name('admin.attributetype.create');
    Route::post('attributetype/create', 'AttributeController@store_type')->name('admin.attributetype.store');
    Route::post('product/attribute/{pid}', 'AttributeController@getCategoryAttribute')->name('admin.attributetype.ajax');
    //Order Route
    Route::get('order', 'OrderController@index')->name('admin.order');
    Route::get('order/create', 'OrderController@create')->name('admin.order.create');
    Route::get('order/approve/{order_id}', 'OrderController@approve')->name('admin.order.approve');
    Route::get('order/pay/{order_code}', 'OrderController@payment')->name('admin.order.pay');

});


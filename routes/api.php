<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('auth/login','Api\AuthController@login');
Route::group(['namespace'=>'Api','middleware'=>'auth:api'],function (){
    Route::group(['prefix'=>'v1','namespace'=>'v1'],function (){
        Route::get('product','ProductsController@index');
        Route::resource('user','UserController');

    });
    Route::group(['prefix'=>'v2','namespace'=>'v2'],function (){
        Route::get('product','ProductsController@index');

    });
});

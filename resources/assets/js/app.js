/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',
    data() {
        return {
            'allAttribute': null
        }
    },
    methods: {
        gettitleId(categoryId) {
            axios.post('attribute/' + categoryId, {}).then(response => {
                this.allAttribute = response.data;


            })
        }
    }

});
$(document).ready(function () {
    $(document).on('click', '.updateCount', function (event) {
        event.preventDefault();
        var _this = $(this);
        var _productId = _this.data('id');
        var _counter = $('.product_counter_' + _productId);
        var _counterValue = parseInt(_counter.text());
        var action = _this.attr('id') == 'plus' ? 'plus' : 'minus';
        var _productSubTotal = $('.product_subtotal_' + _productId);
        var _basketTotal=$('.basket_total');
        axios.post('basket/update', {
            product_id: _productId,
            action: action
        }).then(response => {
            if (response.data.success) {
                _counterValue = action == 'plus' ? _counterValue + 1 : _counterValue - 1;
                _counter.text(_counterValue);
                _productSubTotal.text(response.data.subTotal);
                _basketTotal.text(response.data.total);
            }else{
                alert(response.data.message)
            }


        });


    });


});


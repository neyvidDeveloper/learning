<div class="col-xs-12 col-md-4 col-lg-3">
    <div class="products" >
        <div class="product_img" style="text-align: center">
            <a href="{{ route('product.single',[$products->product_id]) }}">
            <img src="http://via.placeholder.com/150x250" alt="Picture Of product">
            </a>

        </div>
        <div class="product_title"  style="text-align: center">
            {{ $products->product_title }}
        </div>
        <div class="product_price" style="text-align: center">
            {{ $products->product_price }}
        </div>
        <div class="basket_button" style="text-align: center">
            <a href="{{ route('basket.add',[$products->product_id]) }}" class="btn btn-success">اضاقه کردن به سبد خرید</a>
        </div>
    </div>


</div>
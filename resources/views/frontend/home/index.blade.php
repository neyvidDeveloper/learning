@extends('layouts.frontend')

@section('content')
    <div class="container">
        <div class="row">
            @each('frontend.partials.productShow',$products,'products')

        </div>
    </div>
@endsection
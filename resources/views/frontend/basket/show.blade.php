@include('layouts.frontend')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">{{ $title }}‌</div>
            <div class="panel-body">

                <table class="table table-bordered">
                    <tr>
                        <th>شماره</th>
                        <th>نام محصول</th>
                        <th>قیمت</th>
                        <th>تعداد</th>
                        <th>قیمت کل</th>
                    </tr>
                    @if($basket && count($basket)>0)

                        @foreach($basket as $product_id=>$info)
                                <tr>
                                    <td>
                                        {{ $info['product_item']->product_id }}
                                    </td>

                                    <td>
                                        {{ $info['product_item']->product_title }}
                                    </td>
                                    <td>
                                        {{ $info['product_item']->product_price }}
                                    </td>
                                    <td>
                                        <a class="updateCount" id="plus" data-id="{{ $info['product_item']->product_id }}">
                                            <i class="fas fa-plus-square"></i>
                                        </a>
                                        <p class="product_counter_{{$info['product_item']->product_id}}">{{ $info['count'] }}</p>

                                        <a class="updateCount" id="minus" data-id="{{ $info['product_item']->product_id }}">
                                            <i class="fas fa-minus-square"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="product_subtotal_{{$info['product_item']->product_id}}" >
                                            {{ $info['count']*$info['product_item']->product_price }}
                                        </a>

                                    </td>
                                </tr>

                        @endforeach
                            <a class="basket_total">
                                جمع کل خرید:
                                {{ \App\Services\Basket\BasketService::total() }}
                            </a>
                    @endif

                </table>
            </div>
        </div>
    </div>
</div>
@extends('layouts.frontend')
@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $title }}‌</div>
                <div class="panel-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">نام و نام خانوادگی</label>
                    <div class="col-sm-10">
                        {{--{{ $errors->has('user_name') ? 'نام  را وارد کنید' : ''}}--}}
                        <input type="text" name="user_name" class="form-control" id="name" placeholder="Name And Lastname">
                    </div>
                </div>  <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">نام کاربری</label>
                    <div class="col-sm-10">
                        <input type="email" name="user_email" class="form-control" id="email" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">رمز عبور</label>
                    <div class="col-sm-10">
                        <input type="password" name="user_password" class="form-control" id="password"
                               placeholder="Password">
                    </div>
                </div>
                {{--<div class="form-group">--}}
                {{--<div class="col-sm-offset-2 col-sm-10">--}}
                {{--<div class="checkbox">--}}
                {{--<label>--}}
                {{--<input type="checkbox"> Remember me--}}
                {{--</label>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">ثبت نام</button>
                    </div>
                </div>
            </form>
                </div></div>
        </div>
    </div>

@endsection

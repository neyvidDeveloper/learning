@extends('layouts.frontend')

@section('content')


    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="product_image">
                    <img src="{{ $productInfo->product_thumbnail }}" alt="">
                </div>
                <div class="product_desc">
                    <p>
                        {{ $productInfo->product_description }}
                    </p>
                </div>
                <div class="add_to_cart">
                    <a class="btn btn-success" href="{{ route('basket.add',[$productInfo->product_id]) }}">اضافه کردن به سبد خرید</a>
                </div>
            </div>
        </div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" ><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                <li role="presentation" ><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                <li role="presentation" ><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">zzxxzxz</div>
                <div role="tabpanel" class="tab-pane" id="profile">assasa</div>
                <div role="tabpanel" class="tab-pane" id="messages">ewewqwqwq</div>
                <div role="tabpanel" class="tab-pane" id="settings">..qwwqqwwq.</div>
            </div>

    </div>

@endsection

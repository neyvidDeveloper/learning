<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>سفارش جدی ثبت شد</title>
</head>
<body>
<p>
    شناسه سفارش:
    {{ $order->order_code }}
</p>
<p>
    مبلغ کل پرداخت:
    {{ $order->order_total_amount }}


</p>
<p>
    روش پرداخت :
    {{ $order->paymentmethod }}

</p>
</body>
</html>
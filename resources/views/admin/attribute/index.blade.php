@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ $title }}

                    </h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="جستجو">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>شماره</th>
                            <th>عنوان ویژگی</th>
                            <th>نوع ویژگی</th>
                            <th>دسته ویژگی</th>
                            <th>عملیات</th>
                        </tr>

                        @if($attributes && count($attributes)>0)
                            @foreach($attributes as $attribute)
                                <tr>
                                    <td>{{ $attribute->attribute_id }}</td>
                                    <td>{{ $attribute->attribute_name }}</td>
                                    <td>
                                        @if($attribute->attribute_type_id == '0')
                                            اصلی
                                        @else
                                            {{ $attribute->Type->attribute_type_title }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ $attribute->Category->attribute_category_title }}
                                    </td>
                                    <td>
                                        <a>ویرابش</a>
                                        <a href="">حذف</a>
                                    </td>

                                </tr>

                            @endforeach

                        @endif


                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection


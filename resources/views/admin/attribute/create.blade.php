@extends('layouts.admin')

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $title }}</h3>9
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <form role="form" method="post">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="attribute_name">عنوان ویژگی :</label>
                            <input type="text" class="form-control" id="attribute_name" name="attribute_name"
                                   placeholder="عنوان ویژگی را وارد نمایید">
                        </div>
                        <div class="form-group">
                            <label for="attribute_type_id">نوع ویژگی :</label>
                            <select name="attribute_type_id" id="attribute_type_id">
                                <option value="0">اصلی</option>
                                @if($allTypes && count($allTypes)>0)
                                    @foreach($allTypes as $type)
                                        <option value="{{ $type->attribute_type_id }}">{{ $type->attribute_type_title }}</option>
                                    @endforeach
                                @endif

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="attribute_category_id">دسته ویژگی :</label>
                            <select name="attribute_category_id" id="attribute_category_id">
                                <option value="0">بدون دسته بندی</option>
                                @if($allAttrCategory && count($allAttrCategory)>0)
                                    @foreach($allAttrCategory as $attrCategory)
                                        <option value="{{ $attrCategory->attribute_category_id }}">{{ $attrCategory->attribute_category_title }}</option>
                                    @endforeach
                                @endif

                            </select>
                        </div>


                        {{--<div class="form-group">--}}
                        {{--<label for="exampleInputFile">ارسال فایل</label>--}}
                        {{--<input type="file" id="exampleInputFile">--}}

                        {{--<p class="help-block">متن راهنما</p>--}}
                        {{--</div>--}}

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ثبت</button>
                    </div>
                </form>
            </div>

@endsection
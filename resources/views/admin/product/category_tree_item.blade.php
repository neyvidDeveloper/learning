<li>
    @if(isset($product_category_relation[$category->category_id]))
        <input type="checkbox" name="product_category[]" class="minimal-red" value="{{ $category->category_id }}"
               checked>
    @else
        <input @click="gettitleId({{ $category->category_id }})"  type="radio" name="product_category" class="minimal-red" value="{{ $category->category_id }}">
    @endif

    <label>{{ $category->category_title }}</label>
    @if(isset($categories[$category->category_id]))
        @include('admin.product.category_tree',['category_tree'=>$categories[$category->category_id]])
    @endif
</li>
@if($category_tree && count($category_tree)>0)
    <ul style="list-style: none" >
        @foreach($category_tree as $category)
            @include('admin.product.category_tree_item')
        @endforeach
    </ul>
@endif
@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{ $title }}</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="جستجو">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>شماره</th>
                            <th>نام محصول</th>
                            <th>قیمت</th>
                            <th>موجودی انبار</th>
                            <th>درصد تخفیف</th>
                            <th>نوع محصول</th>
                            <th>تعداد کوپن</th>
                            <th>وضعیت</th>
                            <th>توضیحات</th>
                            <th>عملیات</th>
                        </tr>
                        @if($product_items && count($product_items)>0)
                            @foreach($product_items as $product)
                                <tr>
                                    <td>{{ $product->product_id }}</td>
                                    <td>{{ $product->product_title }}</td>
                                    <td>{{ $product->present()->price_in_hezar_toman }}</td>
                                    <td>{{ $product->product_stock }}</td>
                                    <td>{{ $product->present()->product_discount_whit_precent_character }}</td>
                                    <td>{{ $product->product_type}}</td>
                                    <td>{{ $product->product_coupon_count}}</td>
                                    <td>{{ $product->present()->show_status }}</td>
                                    <td>{{ $product->product_description}}</td>
                                    <td>
                                        <a href="{{ route('admin.product.edit').'?id='.$product->product_id }}">ویرابش</a>
                                        <a href="{{ route('admin.product.delete').'?pid='.$product->product_id }}">حذف</a>
                                    </td>
                                    {{--<td><span class="label label-success">تایید شده</span></td>--}}

                                </tr>

                            @endforeach

                        @endif


                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection


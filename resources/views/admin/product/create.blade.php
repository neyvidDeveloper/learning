@extends('layouts.admin')

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $title }}</h3>
                    @if (session('success'))
                        <div class="alert alert-success">
                            <p>محصول مورد نظربا موفقیت ثبت شد.</p>
                        </div>
                    @endif
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="product_title">عنوان محصول :</label>
                            <input type="text" class="form-control" id="product_title" name="product_title"
                                   placeholder="عنوان محصول">
                        </div>
                        <div class="form-group">
                            <label for="product_price">قیمت :</label>
                            <input type="text" class="form-control" id="product_price" name="product_price"
                                   placeholder="قیمت محصول به تومان">
                        </div>
                        <div class="form-group">
                            <label for="product_discount">میزان تخفیف :</label>
                            <input type="text" class="form-control" id="product_discount" name="product_discount"
                                   placeholder="درصد تخفیف راوارد نمایید">
                        </div>
                        <div class="form-group">
                            <label for="product_type">نوع محصول :</label>
                            <select name="product_type" id="product_type">
                                <option value="1">فیزیکی</option>
                                <option value="2">مجازی</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="product_stock">موجودی انبار :</label>
                            <input type="number" class="form-control" id="product_stock" name="product_stock"
                                   placeholder="موجودی را وارد نمایید">
                        </div>
                        <div class="form-group">
                            <label for="product_coupon_count">تعداد کوپن تخفیف :</label>
                            <input type="number" class="form-control" id="product_coupon_count"
                                   name="product_coupon_count"
                                   placeholder="تعداد کوپن را وارد نمایید">
                        </div>
                        <div class="form-group">
                            <label for="product_description">توضیحات محصول :</label>
                            <textarea name="product_description" id="product_description" cols="191"
                                      rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="product_category">دسته محصول :</label>
                            @include('admin.product.category_tree',['category_tree'=>$categories[0]])
                        </div>
                        <div id="allAttr" class="form-group">
                            <div v-if="allAttribute" v-html="allAttribute">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_category">تصویر محصول :</label>
                            <input type="file" name="product_file">
                        </div>
                        {{--<div class="form-group">--}}
                        {{--<label for="exampleInputFile">ارسال فایل</label>--}}
                        {{--<input type="file" id="exampleInputFile">--}}

                        {{--<p class="help-block">متن راهنما</p>--}}
                        {{--</div>--}}
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="product_visible">نمایش داده شود؟
                            </label>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>
            </div>

@endsection
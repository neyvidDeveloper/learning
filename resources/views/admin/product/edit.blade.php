@extends('layouts.admin')

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $title }}</h3>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="product_title">عنوان محصول :</label>
                            <input type="text" class="form-control" id="product_title" name="product_title"
                                   value="{{ $productInfo->product_title }}">
                        </div>
                        <div class="form-group">
                            <label for="product_price">قیمت :</label>
                            <input type="text" class="form-control" id="product_price" name="product_price"
                                   value="{{ $productInfo->product_price }}">
                        </div>
                        <div class="form-group">
                            <label for="product_discount">میزان تخفیف :</label>
                            <input type="text" class="form-control" id="product_discount" name="product_discount"
                                   value="{{ $productInfo->product_discount }}">
                        </div>
                        <div class="form-group">
                            <label for="product_type">نوع محصول :</label>
                            <select name="product_type" id="product_type">
                                {{--@if($productInfo->product_stock == 1)--}}
                                <option value="1">فیزیکی</option>
                                <option value="2">مجازی</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="product_stock">موجودی انبار :</label>
                            <input type="number" class="form-control" id="product_stock" name="product_stock"
                                   value="{{ $productInfo->product_stock }}">
                        </div>
                        <div class="form-group">
                            <label for="product_coupon_count">تعداد کوپن تخفیف :</label>
                            <input type="number" class="form-control" id="product_coupon_count"
                                   name="product_coupon_count"
                                   value="{{ $productInfo->product_coupon_count }}">
                        </div>
                        <div class="form-group">
                            <label for="product_description">توضیحات محصول :</label>
                            <textarea name="product_description" id="product_description" cols="191"
                                      rows="10">{{ $productInfo->product_description }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="product_category">دسته محصول :</label>
                            @include('admin.product.category_tree',['category_tree'=>$categories[0]])
                        </div>


                        {{--<div class="form-group">--}}
                        {{--<label for="exampleInputFile">ارسال فایل</label>--}}
                        {{--<input type="file" id="exampleInputFile">--}}

                        {{--<p class="help-block">متن راهنما</p>--}}
                        {{--</div>--}}
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="product_visible">نمایش داده شود؟
                            </label>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>
            </div>

@endsection
@extends('layouts.admin')

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $title }}</h3>
                    @if (session('success'))
                        <div class="alert alert-success">
                          {{ session('success') }}
                        </div>
                    @endif
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <form role="form" method="post">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="attribute_category_title">عنوان دسته بندی :</label>
                            <input type="text" class="form-control" id="attribute_category_title" name="attribute_category_title"
                                   placeholder="نام دسته بندی ویژگی را وارد نمایید">
                        </div>



                        {{--<div class="form-group">--}}
                        {{--<label for="exampleInputFile">ارسال فایل</label>--}}
                        {{--<input type="file" id="exampleInputFile">--}}

                        {{--<p class="help-block">متن راهنما</p>--}}
                        {{--</div>--}}

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ثبت</button>
                    </div>
                </form>
            </div>

@endsection
@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ $title }}   </h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="جستجو">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>شماره</th>
                            <th>عنوان دسته بندی</th>
                            <th>زیرگروه دسته بندی</th>
                            <th>عملیات</th>
                        </tr>
                        @if($category_item && count($category_item)>0)
                            @foreach($category_item as $category)
                                <tr>
                                    <td>{{ $category->category_id }}</td>
                                    <td>{{ $category->category_title }}</td>
                                    <td>
                                        {{ isset($category->parent) ? $category->parent->category_title : 'دسته مادر' }}

                                    </td>

                                    <td>
                                        <a href="{{ route('admin.category.edit').'?catid='.$category->category_id }}">ویرابش</a>
                                        {{--<a href="{{ route('admin.category.delete').'?catid='.$category->category_id }}">حذف</a>--}}
                                    </td>
                                    <td><span class="label label-success">تایید شده</span></td>

                                </tr>

                            @endforeach

                        @endif


                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection


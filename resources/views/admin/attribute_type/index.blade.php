@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ $title }}   </h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="جستجو">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>شماره</th>
                            <th>عنوان نوع ویژگی</th>
                            <th>عملیات</th>
                        </tr>
                        @if($allTypes && count($allTypes)>0)
                            @foreach($allTypes as $type)
                                <tr>
                                    <td>{{ $type->attribute_type_id }}</td>
                                    <td>{{ $type->attribute_type_title }}</td>


                                    <td>
                                        <a href="">ویرابش</a>
                                        {{--<a href="{{ route('admin.category.delete').'?catid='.$category->category_id }}">حذف</a>--}}
                                    </td>
                                    <td><span class="label label-success">تایید شده</span></td>

                                </tr>

                            @endforeach

                        @endif


                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection


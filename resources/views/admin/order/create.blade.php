@extends('layouts.admin')

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $title }}</h3>
                    @if (session('success'))
                        <div class="alert alert-success">
                            <p>دسته بندی جدید با موفقیت ثبت گردید</p>
                        </div>
                    @endif
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <form role="form" method="post">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="category_title">عنوان دسته بندی :</label>
                            <input type="text" class="form-control" id="category_title" name="category_title"
                                   placeholder="عنوان دسته بندی را وارد نمایید">
                        </div>
                        <div class="form-group">
                            <label for="category_sub">محصول :</label>
                            {{--<select name="category_sub" id="category_sub">--}}
                                {{--<option value="0">اصلی</option>--}}
                                {{--@if($category_childs && count($category_childs)>0)--}}
                                    {{--@foreach($category_childs as $child)--}}
                                        {{--<option value="{{ $child->category_id }}">{{ $child->category_title }}</option>--}}

                                    {{--@endforeach--}}
                                {{--@endif--}}

                            {{--</select>--}}
                        </div>
                        <div class="form-group">
                            <label for="attributes">ویژگی ها:</label>
                            {{--@if($attributes && count($attributes) >0)--}}
                                {{--@foreach($attributes as $attribute)--}}
                                    {{--<input type="checkbox" id="attribute" name="attribute_item[]" value="{{$attribute->attribute_id}}">{{$attribute->attribute_name}}--}}
                                {{--@endforeach--}}
                            {{--@endif--}}

                        </div>


                        {{--<div class="form-group">--}}
                        {{--<label for="exampleInputFile">ارسال فایل</label>--}}
                        {{--<input type="file" id="exampleInputFile">--}}

                        {{--<p class="help-block">متن راهنما</p>--}}
                        {{--</div>--}}

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ثبت</button>
                    </div>
                </form>
            </div>

@endsection
@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ $title }}   </h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="جستجو">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>کاربر</th>
                            <th>مبلغ کل سفارش</th>
                            <th>کل تخفیف</th>
                            <th>مبلغ قابل پرداخت</th>
                            <th>شیوه پرداخت</th>
                            <th>نوع حمل و نقل</th>
                            <th>هزینه حمل و نقل</th>
                            <th>هزینه بسته بندی</th>
                            <th>تعداد آیتم ها سفارش</th>
                            <th>توضیحات</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>

                        </tr>
                        @if($orders && count($orders)>0)
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $order->user->name }}</td>
                                    <td>{{ $order->order_total_amount }}</td>
                                    <td>{{ $order->order_discount }}</td>
                                    <td>{{ $order->order_payable_amount }}</td>
                                    <td>{{ $order->present()->payment_method }}</td>
                                    <td>{{ $order->order_shipping_method }}</td>
                                    <td>{{ $order->order_shipping_amount }}</td>
                                    <td>{{ $order->order_packing_amount }}</td>
                                    <td>{{ $order->order_items->count()}}</td>

                                    <td>{{ $order->order_description }}</td>
                                    <td>{!!  $order->present()->orderStatusHtml !!}</td>
                                    <td>
                                        <a href="">ویرابش</a>
                                        <a href="">حذف</a>
                                    </td>
                                    {!!  $order->present()->opration !!}
                                </tr>

                            @endforeach

                        @endif


                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection


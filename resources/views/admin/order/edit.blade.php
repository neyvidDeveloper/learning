@extends('layouts.admin')

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $title }}</h3>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="category_title">عنوان دسته بندی :</label>
                            <input type="text" class="form-control" id="category_title" name="category_title"
                                   value="{{ $category_info->category_title }}">
                        </div>

                        <div class="form-group">
                            <label for="category_parent_id">سرگروه دسته :</label>
                            <select name="category_parent_id" id="category_parent_id">
                                <option selected
                                value="0">دسته مادر</option>
                                @foreach($all_category as $category)
                                    @if(isset($category_info->parent)  && $category_info->parent->category_id==$category->category_id)
                                        <option selected value=" {{ $category->category_id }}">
                                            {{ $category->category_title }}
                                        </option>
                                    @else
                                        <option  value=" {{ $category->category_id }}">
                                            {{ $category->category_title }}
                                        </option>
                                    @endif

                                @endforeach


                            </select>

                        </div>


                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ثبت</button>
                    </div>
                </form>
            </div>

@endsection